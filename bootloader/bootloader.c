#include "stm8s.h"

// Needs to match UBC
#define BOOT_ADDR 0x8400

// Medium density is 128, low density is 64
#define BLOCK_SIZE 128

void main() 
{

    // Jump to app code
    __asm jpf BOOT_ADDR  __endasm;
}

// Link all the interrupt vectors from our table to 
void usart1_rx_irq(void) __interrupt(28)
{
     __asm jpf 0x8478 __endasm;
}
