#include "stm8s.h"

#define LED_DBG_GPIO (GPIOB)
#define LED_DBG 5

#define DBG_GPIO (GPIOC)
#define DBG 3

#define TX_GPIO (GPIOD)
#define TX 3

#define RX_GPIO (GPIOD)
#define RX 2

// Total memory per task is 8 bytes
typedef struct
{
   // Pointer to the task (must be a 'void (void)' function)
   void (*pTask)(void);

   // Delay (ticks) until the function will (next) be run
   // - see SCH_Add_Task() for further details
   int8_t Delay;

   // Interval (ticks) between subsequent runs.
   // - see SCH_Add_Task() for further details
   int8_t Period;

   // Incremented (by scheduler) when task is due to execute
   int8_t RunMe;

   // Set to 1 if task is co-operative
   // Set to 0 if task is pre-emptive
   int8_t Co_op;
} sTaskH;

enum sercom_state
{
    IDLE,
    START,
    SNC,
    P1,
    P2
};

// TX state machine data
struct
{
    enum sercom_state state;
    uint8_t tx_inprogress;
    uint8_t done;
    uint8_t data;
    uint8_t tx_request;
    uint8_t bit_n;
    uint8_t bit_val;
} tx_sm = {IDLE, 0, 0, 0, 0, 0, 0};

// RX state machine data
struct
{
    enum sercom_state state;
    uint8_t rx_inprogress;
    uint8_t done;
    uint8_t data;
    uint8_t bit_n;
    uint8_t cnt;
} rx_sm = {IDLE, 0, 0, 0, 0, 0};

// The array of tasks
#define hSCH_MAX_TASKS 3
sTaskH hSCH_tasks_G[hSCH_MAX_TASKS];

void set_tx(uint8_t tx_state);
void set_dbg(uint8_t dbg_state);
void set_dbg_led(uint8_t dbg_state);
uint8_t get_rx();
void printf_uart(char *message);
void tx_state_machine(void);
void rx_state_machine(void);

void tx_rx_state_machine(void)
{
    tx_state_machine();
    rx_state_machine();
}

void tx_state_machine(void)
{
    switch(tx_sm.state)
    {
        case IDLE:
            tx_sm.tx_inprogress = 0;
            set_tx(1);

            if(tx_sm.tx_request == 1)
            {
                tx_sm.bit_n = 8;
                tx_sm.tx_inprogress = 1;
                tx_sm.state = START;
            }
        break;

        case START:
            tx_sm.bit_val = 0;
            tx_sm.done = 0;
            tx_sm.state = SNC;
        break;

        case SNC:
            set_tx(tx_sm.bit_val);
            tx_sm.state = P1;
        break;

        case P1:
            tx_sm.state = P2;
        break;

        case P2:
            if((tx_sm.bit_n > 0) && (tx_sm.done == 0)) // Next bit
            {
                tx_sm.bit_n--;
                tx_sm.bit_val = (tx_sm.data & (1 << tx_sm.bit_n)) >> tx_sm.bit_n;
                tx_sm.state = SNC;
            }
            else if((tx_sm.bit_n == 0) && (tx_sm.done == 0)) // Stop bit
            {
                tx_sm.bit_val = 1;
                tx_sm.done = 1;
            }
            else if(tx_sm.done == 1) // Done
            {
                tx_sm.state = IDLE;
            }
        break; 
    }
}

void rx_state_machine(void)
{
    char array[2] = {0, 0};
    switch(rx_sm.state)
    {
        case IDLE:
            if(get_rx() == 0)
            {
                rx_sm.bit_n = 8;
                rx_sm.rx_inprogress = 1;
                rx_sm.cnt = 0;
                rx_sm.state = START;
            }
        break;

        case START:
            rx_sm.done = 0;
            rx_sm.data = 0;
            rx_sm.cnt++;

            if(rx_sm.cnt != 3)
            {
                rx_sm.state = START;
            }
            else
            {
                rx_sm.state = SNC;
            }

        break;

        case SNC:
            rx_sm.state = P1;
        break;

        case P1:
            rx_sm.bit_n--;
            rx_sm.data |= (get_rx() & 1) << rx_sm.bit_n;
            rx_sm.state = P2;
        break;

        case P2:
            if(rx_sm.bit_n == 0)
            {
                rx_sm.state = IDLE;

                array[0] = rx_sm.data;
                printf_uart(array);
            }
            else
            {
                rx_sm.state = SNC;
            }
        break;
    }
}

void printf_uart(char *message)
{
    char *ch = message;
    while (*ch)
    {
        UART1->DR = (unsigned char) *ch;     		//  Put the next character into the data transmission register.
        while ((UART1->SR & UART1_SR_TXE) == 0);   	//  Wait for transmission to complete.
        ch++;                               		//  Grab the next character.
    }
}

uint8_t setup_uart(void)
{
    UART1->BRR2 = 0x00;      //  Set the baud rate registers to 115200 baud
    UART1->BRR1 = 0x02;      //  based upon a 16 MHz system clock.
    //
    //  Disable the transmitter and receiver.
    //
    UART1->CR2 &= ~(UART1_CR2_TEN);      //  Disable transmit.
    UART1->CR2 &= ~(UART1_CR2_REN);      //  Disable receive.
    //
    //  Set the clock polarity, lock phase and last bit clock pulse.
    //
    UART1->CR3 |= UART1_CR3_CPOL & 0xFF;
    UART1->CR3 |= UART1_CR3_CPHA & 0xFF;
    UART1->CR3 |= UART1_CR3_LBCL & 0xFF;
    //
    //  Turn on the UART transmit, receive and the UART clock.
    //
    UART1->CR2 |= UART1_CR2_TEN & 0xFF;
    UART1->CR2 |= UART1_CR2_REN & 0xFF;
    UART1->CR3 |= UART1_CR3_CKEN & 0xFF;

    return 0;
}

uint8_t setup_gpio(void)
{
    LED_DBG_GPIO->DDR |= (1 << LED_DBG); 
    LED_DBG_GPIO->CR1 |= (1 << LED_DBG); 

    DBG_GPIO->DDR |= (1 << DBG);
    DBG_GPIO->CR1 |= (1 << DBG);

    TX_GPIO->DDR |= (1 << TX);
    TX_GPIO->CR1 |= (1 << TX);

    // Input with pullup
    RX_GPIO->DDR &= ~(1 << RX);
    RX_GPIO->CR1 |= (1 << RX);

    return 0;
}

uint8_t setup_timer4(void)
{
    // Assumes 16MHz clock
    // Divides by 64 to get 250KHz
    TIM4->PSCR = 0b110;

    // Reload reg is 25 for 10KHz tick
    TIM4->ARR = 24;
    
    // Enable timer
    TIM4->CR1 |= TIM4_CR1_CEN;

    // Enable interrupt
    TIM4->IER |= TIM4_IER_UIE;

    return 0;
}

uint8_t setup_clock(void)
{
    CLK->ICKR = 0;                          //  Reset the Internal Clock Register
    CLK->ICKR |= CLK_ICKR_HSIEN;            // Enable HSI
    CLK->ECKR = 0;                          //  Disable the external clock.
    while ((CLK->ICKR & CLK_ICKR_HSIRDY) == 0); //  Wait for the HSI to be ready for use.
    CLK->CKDIVR = 0;                        //  Ensure the clocks are running at full speed.
    CLK->PCKENR1 = 0xff;                    //  Enable all peripheral clocks.
    CLK->PCKENR2 = 0xff;                    //  Enable all peripheral clocks.
    CLK->CCOR = 0;                          //  Turn off CCO.
    CLK->HSITRIMR = 0;                      //  Turn off any HSIU trimming.
    CLK->SWIMCCR = 0;                       //  Set SWIM to run at clock / 2.
    CLK->SWR = 0xe1;                        //  Use HSI as the clock source.
    CLK->SWCR = 0;                          //  Reset the clock switch control register.
    CLK->SWCR |= CLK_SWCR_SWEN;             //  Enable switching.
    while ((CLK->SWCR & CLK_SWCR_SWBSY) != 0);  //  Pause while the clock switch is busy.

    /* Configure PE0 as output */
    GPIOE->DDR |= (1 << 0);
    
    /* Push-pull mode, 10MHz output speed */
    GPIOE->CR1 |= (1 << 0);
    GPIOE->CR2 |= (1 << 0);
    
    /* Clock output on PE0 */
    CLK->CCOR |=  0b11111;

    return 0;
}


void set_dbg(uint8_t dbg_state)
{
	if(dbg_state)
	{
        DBG_GPIO->ODR |= (1 << DBG);
    }
    else
    {
        DBG_GPIO->ODR &= ~(1 << DBG);
    }
}

void set_tx(uint8_t tx_state)
{
	if(tx_state)
	{
        TX_GPIO->ODR |= (1 << TX);
    }
    else
    {
        TX_GPIO->ODR &= ~(1 << TX);
    }
}

uint8_t get_rx(void)
{
    if(RX_GPIO->IDR & (1 << RX))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void TIM4_overflow_Handler(void) __interrupt(23)
{
	int8_t Index = 0;

    //printf_uart("he\n");

    // Reset interrupt flag
    TIM4->SR1 &= ~TIM4_SR1_UIF;

  	// NOTE: calculations are in *TICKS* (not milliseconds)
   	for (Index = 0; Index < hSCH_MAX_TASKS; Index++)
   	{
      	// Check if there is a task at this location
      	if (hSCH_tasks_G[Index].pTask)
        {
         	if (hSCH_tasks_G[Index].Delay == 0)
           	{
           		// The task is due to run 
            	if (hSCH_tasks_G[Index].Co_op)
            	{
               		// If it is a co-operative task, increment the RunMe flag
               		hSCH_tasks_G[Index].RunMe += 1;
               	}
            	else
               	{
               		// If it is a pre-emptive task, run it IMMEDIATELY
               		(*hSCH_tasks_G[Index].pTask)();  // Run the task
               		hSCH_tasks_G[Index].RunMe -= 1;   // Reset / reduce RunMe flag
               	}

            	if (hSCH_tasks_G[Index].Period)
               	{  
               		// Schedule periodic tasks to run again
               		hSCH_tasks_G[Index].Delay = hSCH_tasks_G[Index].Period;
            	}
          	}
            else
            {
                // Not yet ready to run: just decrement the delay 
                hSCH_tasks_G[Index].Delay -= 1;
            }
        }
	}
}

void hSCH_Dispatch_Tasks(void)
{
   	int8_t Index;

   	// Dispatches (runs) the next task (if one is ready)
   	for (Index = 0; Index < hSCH_MAX_TASKS; Index++)
	{
     	// Only dispatching co-operative tasks
      	if ((hSCH_tasks_G[Index].Co_op) && (hSCH_tasks_G[Index].RunMe > 0))
    	{
         	(*hSCH_tasks_G[Index].pTask)(); 	// Run the task
        	hSCH_tasks_G[Index].RunMe -= 1;   	// Reset / reduce RunMe flag
    	}
	}
}

int8_t hSCH_Add_Task(void (*Fn_p)(), // Task function pointer
                  	int8_t   Del,    // Num ticks 'til task first runs 
                   	int8_t   Per,    // Num ticks between repeat runs
                   	int8_t   Co_op)  // Co_op / pre_emp
{
   	int8_t Index = 0;

   	// First find a gap in the array (if there is one)
	while ((hSCH_tasks_G[Index].pTask != 0) && (Index < hSCH_MAX_TASKS))
    {
      	Index++;
    }

   	// Have we reached the end of the list?   
   	if (Index == hSCH_MAX_TASKS)
    {
      	// Task list is full
      	// Also return an error code
      	return hSCH_MAX_TASKS;
    }

   	// If we're here, there is a space in the task array
   	hSCH_tasks_G[Index].pTask = Fn_p;

   	hSCH_tasks_G[Index].Delay  = Del;
   	hSCH_tasks_G[Index].Period = Per;

   	hSCH_tasks_G[Index].Co_op = Co_op;

   	hSCH_tasks_G[Index].RunMe  = 0;

   	return Index; // return position of task (to allow later deletion)
}

void set_led(uint8_t led_state)
{
	if(led_state)
	{
        LED_DBG_GPIO->ODR |= (1 << LED_DBG);
    }
    else
    {
        LED_DBG_GPIO->ODR &= ~(1 << LED_DBG);
    }
}

void tx_task(void)
{
    tx_sm.tx_request = 1;
    tx_sm.data = 0x55;
}

void main() 
{
    setup_clock();
    setup_gpio();
    setup_timer4();
    setup_uart();

    // Only preemptive task is sercom state machine
    hSCH_Add_Task(tx_rx_state_machine, 0, 0, 0);
    hSCH_Add_Task(tx_task, 0, 100, 1);

    // Enable interrupts (reset interrupt mask)
    __asm rim __endasm;

    while(1)
    {

        if(get_rx())
        {
            set_led(1);
        }
        else
        {
            set_led(0);
        }

        // Dispatch cooperative tasks
        hSCH_Dispatch_Tasks();

        // Wait for interrupt (sleep)
        __asm wfi __endasm;
    }
}
